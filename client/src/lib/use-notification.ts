import store from "@/store";
import { ref } from "@vue/composition-api";

export function useNotification() {
  // Basic Notifications for ADD UPDATE DELELT
  const isAddSuccessful = ref(false);
  const isDeleteSuccessful = ref(false);
  const isEditSuccessful = ref(false);
  const hasRequestError = ref(false);

  const resetNotification = () => {
    store.commit("application/setHttpErrorMessage", "");
    hasRequestError.value = false;
    isAddSuccessful.value = false;
    isEditSuccessful.value = false;
    isDeleteSuccessful.value = false;
  };

  return {
    isAddSuccessful,
    isEditSuccessful,
    isDeleteSuccessful,
    hasRequestError,
    resetNotification
  };
}
