import { ref, watch } from "@vue/composition-api";

export function useForm() {
  const form = ref();
  const valid = ref(false);
  const email = ref("");
  const loading = ref(false);
  const emailRules = ref([
    v => !!v || "E-mail is required",
    v => /.+@.+\..+/.test(v) || "E-mail must be valid"
  ]);
  const isSuccessed = ref(false);
  const hasHttpError = ref(false);

  // Remove Success/Error notification when start to type, after success/error request
  watch(
    () => email.value,
    () => {
      if (hasHttpError || isSuccessed) {
        isSuccessed.value = false;
        hasHttpError.value = false;
      }
    }
  );
  return { form, valid, emailRules, email, loading, isSuccessed, hasHttpError };
}
