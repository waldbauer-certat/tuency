import Vue from "vue";
import Vuex from "vuex";
import { user } from "./user";
import { application } from "./application";
import { netobject } from "./netobject";
import { contact } from "./contact";
import { organisation } from "./organisation";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    application,
    netobject,
    contact,
    organisation
  }
});
