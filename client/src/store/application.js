/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author(s):
 * * Fadi Abbud <fadi.abbud@intevation.de>
 */
import { version } from "../../package.json";
import { HTTP } from "../lib/http";

export const application = {
  namespaced: true,
  state: () => ({
    httpErrorMessage: "",
    version,
    showSelectNodeDialog: false,
    showRules: false,
    tenants: [],
    contactRoles: [],
    showErrorDialog: false,
    netobjectType: "asn"
  }),
  mutations: {
    setNetobjectType: (state, type) => {
      state.netobjectType = type;
    },
    setHttpErrorMessage: (state, message) => {
      state.httpErrorMessage = message;
    },
    setShowSelectNodeDialog: (state, show) => {
      state.showSelectNodeDialog = show;
    },
    setShowRules: (state, show) => {
      state.showRules = show;
    },
    setTenants: (state, tenants) => {
      state.tenants = tenants;
    },
    setContactRoles: (state, roles) => {
      state.contactRoles = roles;
    },
    setShowErrorDialog: (state, value) => {
      state.showErrorDialog = value;
    }
  },
  getters: {
    // To make changes on the version number form
    appVersion: state => {
      return state.version + " rev" + process.env.VUE_APP_GIT_REV || "0";
    }
  },
  actions: {
    getTenants({ commit }) {
      return new Promise((resolve, reject) => {
        HTTP.get("/tenants")
          .then(response => {
            commit("setTenants", response.data);
            resolve(response);
          })
          .catch(error => {
            reject(error);
          });
      });
    }
  }
};
