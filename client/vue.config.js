module.exports = {
  transpileDependencies: ["vuetify"],
  devServer: {
    /* We need to check the value here as the environment variables are parsed as a String */
    disableHostCheck:
      process.env.VUE_APP_DISABLE_HOST_CHECK === "true" ? true : false,
    proxy: {
      "^/api": {
        target: process.env.VUE_BACKEND_API_URL,
        changeOrigin: true,
        ws: true,
        logLevel: "debug"
      }
    }
  }
};
