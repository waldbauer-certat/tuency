# Backend

This folder contains scripts to install the tuency php backend including the necessary dependencies. Additionally a configuration script is provided.

## Setup

To set up the tuency backend application, the setup.sh shell script can be used. This will install all necessary dependencies and set up an nginx webserver to serve the php application.

For usage information use:

```bash
cd /path/to/tuency/repo/deployment/backend
./setup.sh -h
# Usage: ./setup.sh [-c COMPOSER_DIR] [-p BACKEND_PATH] [-y] [-h] [--backend-port BACKEND_PORT] [--db-host=DB_HOST] [--db-port=DB_PORT] [--keycloak-url=KEYCLOAK_URL] [--keycloak-admin-realm=KEYCLOAK_ADMIN_REALM] [--keycloak-admin-client=KEYCLOAK_ADMIN_CLIENT] [--keycloak-tuency-realm=KEYCLOAK_TUENCY_REALM] [--keycloak-tuency-client=KEYCLOAK_TUENCY_CLIENT]
#   -c COMPOSER_DIR Path to install composer to, defaults to /usr/local/bin
#   -p BACKEND_PATH Path to backend application, defaults to ../../
#   -y Assume yes for all prompts and run non-interactively
#   -h Show this usage information
#   --backend-port Port the backend should listen to, defaults to 8070
#   --db-host Set the database host, defaults to localhost
#   --db-port Set the database port, defaults to 5432
#   --db-name Set the database name, defaults to tuencydb
#   --keycloak-url Set the url of the used KeyCloak instance
#   --keycloak-admin-realm Set the realm used for admin access
#   --keycloak-admin-client Set the client used for admin access
#   --keycloak-tuency-realm Set the tuency realm
#   --keycloak-tuency-client Set the tuency client id
#   --keycloak-url Set the url of the used KeyCloak instance, defaults to localhost
```

To run the setup use:

```bash
cd /path/to/tuency/repo/deployment/backend
./setup.sh -y \
    -c /path/to/install/composer \
    -p /path/backend/application \
    --backend-port 8070 \
    --db-host myDBHost.example --db-port 5432 --db-name tuencydb\
    --keycloak-url myKCHost.example \
    --keycloak-admin-realm adminRealm \
    --keycloak-admin-client adminClient \
    --keycloak-tuency-realm tuencyRealm \
    --keycloak-tuency-client tuencyClient
```

_Note: Ensure that the ```www-data``` user has read and write permission in the backend directory._

## Configuration

The application is configured during the initial setup. Changes to the configuration can be made using the config.sh shell script.

For usage information use:

```bash
cd /path/to/tuency/repo/deployment/backend
./config.sh -h
# Usage: ./config.sh --api-key=API_KEY--db-host=DB_HOST --db-port=DB_PORT --db-name=DB_NAME --keycloak-url=KEYCLOAK_URL --keycloak-admin-realm=KEYCLOAK_ADMIN_REALM --keycloak-admin-client=KEYCLOAK_ADMIN_CLIENT --keycloak-tuency-realm=KEYCLOAK_TUENCY_REALM --keycloak-tuency-client=KEYCLOAK_TUENCY_CLIENT [-p BACKEND_PATH] [-h]
#   --api-key Set the API to use
#   --db-host Set the database host
#   --db-port Set the database port
#   --db-name Set the database name
#   --keycloak-url Set the url of the used KeyCloak instance
#   --keycloak-admin-realm Set the realm used for admin access
#   --keycloak-admin-client Set the client used for admin access
#   --keycloak-tuency-realm Set the tuency realm
#   --keycloak-tuency-client Set the tuency client id
#   -p Path to backend application, defaults to ../../
#   -h Show this usage information
```

To run a configuration use:

```bash
./config.sh --db-host myDBHost.example --db-port 5432 --db-name tuencydb \
   --api-key A1B2C3
   --keycloak-url myKCHost.example \
   --keycloak-admin-realm adminRealm --keycloak-admin-client adminClient \
   --keycloak-tuency-realm tuencyRealm --keycloak-tuency-client tuencyClient \
   -p /path/to/backend/application
```

### Customizing email notifications

To customize email notifications two files can be edited:

* The email view in [backend/resources/views/emails](backend/resources/views/emails)
* The mailable class in [backend/app/Mail](backend/app/Mail)

The view blade template contains the actual mail text and variables indicated by `{{ VARNAME }}` that can be filled using the mailable class.
As the mails are customized per constituency, each constituency needs its own set of mail templates.
E.g. mails for the constituency "example" will be sent using the templates `configreminder_example.blade.php`, `claimupdated_example.blade.php` etc.
If the constituency name contains `.` characters, these need to be replaced by `_`, e.g.:
Mails for the constituency `example.org` will be sent using the template `configreminder_example_org.blade.php`.

The mailable class is used to create the mail from the template defined by the view und fill the template variables.

## Update

The backend application update requires three steps: Updating the application code, updating the database und updating the config.

To update the application code, copy the updated files into the backend directory.

Database updates can be applied using

```bash
php artisan migrate
```

The config can be cached again using

```bash
php artisan config:cache
```


## Docker Setup

The [Dockerfile](Dockerfile) can be used to set up a container providing the backend application:

Build image:

```bash
cd /path/to/tuency/repo
docker build -t tuency/backend -f deployment/backend/Dockerfile .
```

Run container:

```bash
docker run --name tuency_backend -d tuency/backend
```

The image uses several environment variables for configuring the backend:

* DB_HOST
* DB_PORT
* DB_NAME
* KEYCLOAK_URL
* KEYCLOAK_ADMIN_REALM
* KEYCLOAK_ADMIN_CLIENT
* KEYCLOAK_TUENCY_REALM
* KEYCLOAK_TUENCY_CLIENT
* BACKEND_PATH
* BACKEND_PORT

These can be set using docker run with the `-e` parameter:

```bash
docker run --name tuency_backend -e DB_HOST=mydbhost ... -d tuency/backend
```

After starting the container, the configuration can be set using

```bash
docker exec -ti tuency_backend bash
cd /usr/src/tuency-installer
./config.sh --db-host $DB_HOST --db-port $DB_PORT --db-name $DB_NAME \
    --api-key MY_TOKEN \
    --keycloak-url $KEYCLOAK_URL \
    --keycloak-admin-realm $KEYCLOAK_ADMIN_REALM --keycloak-admin-client $KEYCLOAK_ADMIN_CLIENT \
    --keycloak-tuency-realm $KEYCLOAK_TUENCY_REALM --keycloak-tuency-client $KEYCLOAK_TUENCY_CLIENT \
    -p $BACKEND_PATH
```

where `MY_TOKEN` is an api token, e.g. generated by `openssl rand -base64 12`.

To allow a connection to the KeyCloak admin api, credentials need to be added to the backend config.
The credentials can be set in the .env file using an terminal editor, e.g. vim, using these properties:

```
KEYCLOAK_ADMIN_USERNAME=...
KEYCLOAK_ADMIN_PASSWORD=...
```
