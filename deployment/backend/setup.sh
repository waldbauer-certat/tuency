#!/bin/bash
#Setup script for the tuency backend application

PROJECT_ROOT=$PWD/../..
BACKEND_ROOT="$PROJECT_ROOT/backend"

BACKEND_PORT=8070

COMPOSER_INSTALL_DIR=/usr/local/bin
COMPOSER_FILENAME=composer

SERVER_BLOCK_SRC=$PWD/tuency_backend
SERVER_BLOCK_DST=/etc/nginx/sites-available/tuency
SERVER_BLOCK_DST_ACTIVE=/etc/nginx/sites-enabled/tuency

export DEBIAN_FRONTEND=noninteractive
PARAMETER_Y=0

#Keycloak connection data
KEYCLOAK_URL=localhost:8090
KEYCLOAK_ADMIN_CLIENT=tuency
KEYCLOAK_ADMIN_REALM=master
KEYCLOAK_TUENCY_CLIENT=tuency
KEYCLOAK_TUENCY_REALM=tuency

#Database connection data
DB_HOST=localhost
DB_PORT=5432
DB_NAME=tuencydb

printUsage()
{
   echo "Usage: $0 [-c COMPOSER_DIR] [-p BACKEND_PATH] [-y] [-h] [--backend-port BACKEND_PORT] [--db-host=DB_HOST] [--db-port=DB_PORT] [--keycloak-url=KEYCLOAK_URL] [--keycloak-admin-realm=KEYCLOAK_ADMIN_REALM] [--keycloak-admin-client=KEYCLOAK_ADMIN_CLIENT] [--keycloak-tuency-realm=KEYCLOAK_TUENCY_REALM] [--keycloak-tuency-client=KEYCLOAK_TUENCY_CLIENT]"
   echo -e "\t-c COMPOSER_DIR Path to install composer to, defaults to /usr/local/bin"
   echo -e "\t-p BACKEND_PATH Path to backend application, defaults to ../../"
   echo -e "\t-y Assume yes for all prompts and run non-interactively"
   echo -e "\t-h Show this usage information"
   echo -e "\t--backend-port Port the backend should listen to, defaults to 8070"
   echo -e "\t--db-host Set the database host, defaults to localhost"
   echo -e "\t--db-port Set the database port, defaults to 5432"
   echo -e "\t--db-name Set the database name, defaults to tuencydb"
   echo -e "\t--keycloak-url Set the url of the used KeyCloak instance"
   echo -e "\t--keycloak-admin-realm Set the realm used for admin access"
   echo -e "\t--keycloak-admin-client Set the client used for admin access"
   echo -e "\t--keycloak-tuency-realm Set the tuency realm"
   echo -e "\t--keycloak-tuency-client Set the tuency client id"
   echo -e "\t--keycloak-url Set the url of the used KeyCloak instance, defaults to localhost"
   exit 1
}

options=$(getopt -o c:p:yh --long backend-port: --long db-host: --long db-port: --long db-name: --long keycloak-url: --long keycloak-admin-realm: --long keycloak-admin-realm: --long keycloak-admin-client: --long keycloak-tuency-realm: --long keycloak-tuency-client: -- "$@")
[ $? -eq 0 ] || {
    printUsage
}

eval set -- "$options"

while true; do
   case "$1" in
      -c )
         shift;
         COMPOSER_INSTALL_DIR="$1"
         ;;
      -p )
            shift;
            BACKEND_ROOT="$1"
            ;;
      -y ) PARAMETER_Y=1 ;;
      -h ) printUsage ;;
      --backend-port)
         shift;
         BACKEND_PORT="$1"
         ;;
      --db-host)
         shift;
         DB_HOST=$1
         ;;
      --db-port)
         shift;
         DB_PORT=$1
         ;;
      --db-name)
         shift;
         DB_NAME=$1
         ;;
      --keycloak-url)
         shift;
         KEYCLOAK_URL=$1
         ;;
      --keycloak-admin-realm)
         shift;
         KEYCLOAK_ADMIN_REALM=$1
         ;;
      --keycloak-admin-client)
         shift;
         KEYCLOAK_ADMIN_CLIENT=$1
         ;;
      --keycloak-tuency-realm)
         shift;
         KEYCLOAK_TUENCY_REALM=$1
         ;;
      --keycloak-tuency-client)
         shift;
         KEYCLOAK_TUENCY_CLIENT=$1
         ;;
      --)
         shift
         break
         ;;
   esac
   shift
done

if [ "$EUID" -ne 0 ]
  then echo "The setup needs root privileges to install the backend dependencies. Please run as root."
  exit 1
fi

echo "Updating package information..."
apt-get update > /dev/null

echo "Installing backend dependencies.."

DEPENDENCIES="git nginx curl php php-fpm php-bcmath php-cli unzip openssl php php-cli php-common php-curl php-json php-mbstring php-xml php-pgsql"

if [ "$PARAMETER_Y" -ne 0 ]
    then
    echo "The following packages will be installed: $DEPENDENCIES"
    apt-get install $DEPENDENCIES -y -qq > /dev/null
    else
    apt-get install $DEPENDENCIES
fi

APT_RESULT=$?

if [ "$APT_RESULT" -ne 0 ]
    then echo "Error: Dependency setup failed"
    exit 1
fi

echo "Using backend directory: $BACKEND_ROOT"

COMPOSER="$COMPOSER_INSTALL_DIR/$COMPOSER_FILENAME"
echo "Installing composer to $COMPOSER..."
EXPECTED_COMPOSER_CHECKSUM="$(php -r 'copy("https://composer.github.io/installer.sig", "php://stdout");')"
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_COMPOSER_CHECKSUM="$(php -r "echo hash_file('sha384', 'composer-setup.php');")"

if [ "$EXPECTED_COMPOSER_CHECKSUM" != "$ACTUAL_COMPOSER_CHECKSUM" ]
then
    >&2 echo 'ERROR: Invalid composer installer checksum'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet --filename=$COMPOSER_FILENAME --install-dir=$COMPOSER_INSTALL_DIR
COMPOSER_RESULT=$?
rm composer-setup.php

if [ "$COMPOSER_RESULT" -ne 0 ]
    then echo "Error: Composer setup failed"
    exit 1
fi

echo "Installing laravel..."
$COMPOSER --quiet global require laravel/installer

echo "Setting up nginx..."
rm /etc/nginx/sites-enabled/default > /dev/null 2>&1
cp $SERVER_BLOCK_SRC $SERVER_BLOCK_DST

echo "Configuring server block at $SERVER_BLOCK_DST"
sed -i 's,BACKEND_ROOT,'"$BACKEND_ROOT"',g' $SERVER_BLOCK_DST
sed -i 's,$BACKEND_PORT,'"$BACKEND_PORT"',g' $SERVER_BLOCK_DST
rm $SERVER_BLOCK_DST_ACTIVE > /dev/null 2>&1
ln -s $SERVER_BLOCK_DST $SERVER_BLOCK_DST_ACTIVE

echo "Configuring application..."
TOKEN=$(openssl rand -base64 12)
./config.sh --db-host $DB_HOST --db-port $DB_PORT --db-name $DB_NAME \
   --api-key $TOKEN \
   --keycloak-url $KEYCLOAK_URL \
   --keycloak-admin-realm $KEYCLOAK_ADMIN_REALM --keycloak-admin-client $KEYCLOAK_ADMIN_CLIENT \
   --keycloak-tuency-realm $KEYCLOAK_TUENCY_REALM --keycloak-tuency-client $KEYCLOAK_TUENCY_CLIENT \
   -p $BACKEND_ROOT

echo "Installing application..."
cd $BACKEND_ROOT
$COMPOSER --quiet update
$COMPOSER --quiet install --optimize-autoloader --no-dev
php artisan key:generate
