#!/bin/bash
#Configure the tuency backend application

export DEBIAN_FRONTEND=noninteractive

PROJECT_ROOT=$PWD/../..
BACKEND_ROOT="$PROJECT_ROOT/backend"

#API key to set in the backend
API_KEY=""

#Keycloak connection data
KEYCLOAK_URL=""
KEYCLOAK_PORT=""
KEYCLOAK_ADMIN_CLIENT=""
KEYCLOAK_ADMIN_REALM=""
KEYCLOAK_TUENCY_CLIENT=""
KEYCLOAK_TUENCY_REALM=""

#Database connection data
DB_HOST=""
DB_PORT=""
DB_NAME=""

printUsage()
{
    echo "Usage: $0 --api-key=API_KEY--db-host=DB_HOST --db-port=DB_PORT --db-name=DB_NAME --keycloak-url=KEYCLOAK_URL --keycloak-admin-realm=KEYCLOAK_ADMIN_REALM --keycloak-admin-client=KEYCLOAK_ADMIN_CLIENT --keycloak-tuency-realm=KEYCLOAK_TUENCY_REALM --keycloak-tuency-client=KEYCLOAK_TUENCY_CLIENT [-p BACKEND_PATH] [-h]"
    echo -e "\t--api-key Set the API to use"
    echo -e "\t--db-host Set the database host"
    echo -e "\t--db-port Set the database port"
    echo -e "\t--db-name Set the database name"
    echo -e "\t--keycloak-url Set the url of the used KeyCloak instance"
    echo -e "\t--keycloak-admin-realm Set the realm used for admin access"
    echo -e "\t--keycloak-admin-client Set the client used for admin access"
    echo -e "\t--keycloak-tuency-realm Set the tuency realm"
    echo -e "\t--keycloak-tuency-client Set the tuency client id"
    echo -e "\t-p Path to backend application, defaults to ../../"
    echo -e "\t-h Show this usage information"
    exit 1
}

options=$(getopt -o p:h --long api-key: --long db-host: --long db-port: --long db-name: --long keycloak-url: --long keycloak-admin-realm: --long keycloak-admin-realm: --long keycloak-admin-client: --long keycloak-tuency-realm: --long keycloak-tuency-client: -- "$@")
[ $? -eq 0 ] || {
    printUsage
}

eval set -- "$options"

while true; do
   case "$1" in
        -p)
            shift;
            BACKEND_ROOT="$1"
            ;;
        -h) printUsage ;;
        --api-key)
            shift;
            API_KEY=$1
            ;;
        --db-host)
            shift;
            DB_HOST=$1
            ;;
        --db-port)
            shift;
            DB_PORT=$1
            ;;
        --db-name)
            shift;
            DB_NAME=$1
            ;;
        --keycloak-url)
            shift;
            KEYCLOAK_URL=$1
            ;;
        --keycloak-admin-realm)
            shift;
            KEYCLOAK_ADMIN_REALM=$1
            ;;
        --keycloak-admin-client)
            shift;
            KEYCLOAK_ADMIN_CLIENT=$1
            ;;
        --keycloak-tuency-realm)
            shift;
            KEYCLOAK_TUENCY_REALM=$1
            ;;
        --keycloak-tuency-client)
            shift;
            KEYCLOAK_TUENCY_CLIENT=$1
            ;;
        --)
            shift
            break
            ;;
   esac
   shift
done

ENV_FILE=$BACKEND_ROOT/.env

if [[ -z "$DB_HOST" || -z "$DB_PORT" || -z "$DB_NAME" \
    || -z "$KEYCLOAK_URL" \
    || -z "$KEYCLOAK_ADMIN_CLIENT" || -z "$KEYCLOAK_ADMIN_REALM" \
    || -z "$KEYCLOAK_TUENCY_CLIENT" || -z "$KEYCLOAK_TUENCY_REALM" ]]
    then echo "Mandatory parameters not set"
    printUsage
fi

echo "Deploying config to $ENV_FILE"
cp tuency_dot_env $ENV_FILE

sed -i 's,\$API_KEY,'"$API_KEY"',g' $ENV_FILE

sed -i 's,\$DB_HOST,'"$DB_HOST"',g' $ENV_FILE
sed -i 's,\$DB_PORT,'"$DB_PORT"',g' $ENV_FILE
sed -i 's,\$DB_NAME,'"$DB_NAME"',g' $ENV_FILE

sed -i 's,\$KEYCLOAK_URL,'"$KEYCLOAK_URL"',g' $ENV_FILE
sed -i 's,\$KEYCLOAK_ADMIN_REALM,'"$KEYCLOAK_ADMIN_REALM"',g' $ENV_FILE
sed -i 's,\$KEYCLOAK_ADMIN_CLIENT,'"$KEYCLOAK_ADMIN_CLIENT"',g' $ENV_FILE
sed -i 's,\$KEYCLOAK_TUENCY_REALM,'"$KEYCLOAK_TUENCY_REALM"',g' $ENV_FILE
sed -i 's,\$KEYCLOAK_TUENCY_CLIENT,'"$KEYCLOAK_TUENCY_CLIENT"',g' $ENV_FILE

cd $BACKEND_ROOT
php artisan config:cache