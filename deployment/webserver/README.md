# Web application

This folder contains scripts to install the tuency VueJS web application including the necessary dependencies.

## Setup

To set up the application, the build.sh and setup.sh shell scripts can be used. This will install all necessary dependencies and set up an OpenRestsy Webserver to serve the application.

Note that if an older version of luarocks is already installed, e.g., via a package manager, it is advised to uninstall this first:

```bash
apt remove luarocks
apt autoremove
```

For usage information use:

```bash
cd /path/to/tuency/repo/deployment/webserver
./build.sh -h
# Usage: ./build.sh [-y] [-h] [--client-dir CLIENT_DIR] [--install-dir INSTALL_DIR]
#         -y Assume yes for all prompts and run non-interactively
#         -h Show this usage information
#         --client-dir Set the directory the tuency client application is present, defaults to ../../client
#         --install-dir Set the directory the tuency application will be installed to, defaults to /usr/local/tuency
./setup.sh -h
# Usage: ./setup.sh [-y] [-h] [--install-dir INSTALL_DIR] [--include-dir INCLUDE_DIR] [--resolver RESOLVER] [--scripts-dir SCRIPTS_DIR]
#   -y Assume yes for all prompts and run non-interactively
#   -h Show this usage information
#   --install-dir Set the directory the tuency application will be installed to, defaults to /usr/local/tuency
#   --include-dir Set the directory containing the tuency server blocks, defaults to /usr/local/openresty/nginx/sites
#   --resolver Set the resolver used by OpenResty, defaults to local
#   --scripts-dir Set the directory where the tuency lua scripts will be placed
```

To run the setup use:

```bash
cd /path/to/tuency/repo/deployment/webserver
./build.sh -y --client-dir /path/to/tuency-client --install-dir /path/to/install/to
./setup.sh -y --client-dir /path/to/tuency-client --install-dir /path/to/install/to --include-dir /path/to/server_blocks --resolver 127.0.0.1 --scripts /path/to/put/scripts/into
```

## Maintaining the constituencies

For one constituency to work:

1. A keycloak-client has to exist, and have a "redirect" url,
   which is the public URL for the constituency.
2. The keycloak-client-id needs to have a matching ngnix server configuration
   entry, to connect the incoming request URL to the right styles
   via redirect and keycloak via its client-id.
3. The database table `tenant` needs to have an entry which name matches
   the an icon file in the `resources/logo/` for all constituencies
   with the pattern `logo-NAME.svg`

### Create a new constituency

* Create keycloak client.
* Add mappers to the client:
   * username
      * Mapper Type: User Attribute
      * User Attribute: username
      * Token Claim Name: username
   * full name
      * Mapper Type: Users' full name
   * clientroles
      * Mapper Type: User Client Roles
      * Client ID: {tuency client name} (see [KeyCloak Readme File](../keycloak/README.md#Add-tuency-realm-and-client) )
      * Multivalued: true
      * Token Claim Name: clientroles
      * Claim JSON Type: String
* Add database entry
* Put a small svg icon with a corresponding filename (see above) in the general resources.

Then create an application instance that can be served by openresty,
use the `create_instance.sh` script.
The new instance can be used for an additional constituency including its own set of resources/icons and a theme.

For usage information use:

```bash
cd /path/to/tuency/repo/deployment/webserver
./create_instance.sh -h
# Usage: ./create_instance.sh -c CLIENT_NAME -r RESOURCE_DIR -s SERVER_NAME --backend-url BACKEND_URL --keycloak-url KEYCLOAK_URL [-p PORT] [-h] [--client-dir CLIENT_DIR] [--install-dir INSTALL_DIR] [--scripts-dir SCRIPTS_DIR]
#   -c Set the client name to use for the instance, mandatory
#   -r Set the resource directory to use for the instance, mandatory
#   -s Set the server name to use for the instance, mandatory
#   -p Set the port to use for the instance, defaults to 80
#   -h Show this usage information
#   --backend-url URL to tuency backend, mandatory
#   --keycloak-url URL to the keycloak instance, mandatory
#   --client-dir Set the directory the built tuency client application is present, defaults to /usr/local/tuency
#   --install-dir Set the directory the configured server block will be installed to, defaults to /usr/local/openresty/nginx/sites
#   --scripts-dir Set the directory where the tuency lua scripts are present, defaults to /usr/local/openresty/nginx/scripts
```

To create an instance use:

```bash
cd /path/to/tuency/repo/deployment/webserver
./create_instance.sh -c myClient -r /path/to/client/resources/ -s https://myClient.example --backend-url https://tuency-backend.example --keycloak-url https://tuency-keycloak.example -p 80 --client-dir /path/to/installed/client --install-dir /path/to/install/cfg/to --scripts-dir /path/to/tuency/lua/scripts
```

This will create an application instances that can be reached under the given server name after a reload:

```bash
/usr/local/openresty/nginx/sbin/nginx -s reload
```



### Create Mail templates

To send mail notifications customized to each constituency, add a new mail template in [backend/resources/views/emails](backend/resources/views/emails).
The application will pick the template using the constituency name, so for a reminder mail from constituency named "exampleconstituency", the template "configreminder_exampleconstituency.blade.php" will be used.

### Change existing constituencies

To edit servername, port or paths, the server block configuration created by the create_instance script can be used.
The file can be found at the path given by the ```--install-dir``` parameter or at the default path ```/usr/local/openresty/nginx/sites```.

Theme colors and icons can be changed by editing the respective css files or by replacing the icon files in the instance resource folder that was defined by the ```-r``` parameter of the create_instance script.
The following files can be used to customize the tuency instance

* logo.png - Constituency icon
* account_style.css - KeyCloak account page styling
* client_style.css - Application styling
* login_style.css - Login page styling

The [example resource folder](docs/examples/tuency_resources) provides some examples for customization options.

## Update

To update the application, the updated source folder can be build again using the build.sh script and can be deployed using the setup.sh script or manually by replacing the files in the installation folder.

## Docker setup

The [Dockerfile](Dockerfile) can be used to set up a container providing the web client application:

Build image:

```bash
cd /path/to/tuency/repo
docker build -t tuency/client -f deployment/webserver/Dockerfile .
```

Run container:

```bash
docker run --name tuency_client -d tuency/client
```

Connect to container and create instances:

```bash
docker exec -ti tuency_client bash
cd /usr/src/tuency-installer
./create_instance.sh -c myClient -r /usr/src/tuency-client/ -s https://myClient.example -p 80 --backend-url https://myBackend.example --keycloak-url https://myKeyCloak.example
/usr/local/openresty/nginx/sbin/nginx -s reload
```
