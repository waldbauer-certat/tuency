# Deployment

This folder contains scripts and documentation to deploy and update the tuency application. Additionally dockerfiles are provided in the respective folders.

Readme files:

* [Database](db/README.md)
* [KeyCloak](keycloak/README.md)
* [Backend](backend/README.md)
* [Webserver](webserver/README.md)
