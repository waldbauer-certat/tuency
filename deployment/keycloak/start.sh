#!/bin/bash
#Start keycloak using standalone script
#Used in the docker setup

${KEYCLOAK_DIR}/keycloak-${KEYCLOAK_VERSION}/bin/standalone.sh -b 0.0.0.0 -bmanagement=0.0.0.0