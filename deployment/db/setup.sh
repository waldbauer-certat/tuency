#!/bin/bash
#Setup script for the tuency db
export DEBIAN_FRONTEND=noninteractive
parameterY=""

printUsage()
{
   echo "Usage: $0 [-y] [-h]"
   echo -e "\t-y Assume yes for all prompts and run non-interactively"
   echo -e "\t-h Show this usage information"
   exit 1 # Exit script after printing help
}

while getopts "yh" opt
do
   case "$opt" in
      y ) parameterY="-y" ;;
      h ) printUsage ;;
      ? ) printUsage ;;
   esac
done

if [ "$EUID" -ne 0 ]
  then echo "The setup needs root privileges to install the postgresql database. Please run as root."
  exit
fi

echo "Updating package information..."
apt-get update > /dev/null
echo "Installing Postgresql database.."
apt-get install $parameterY postgresql

