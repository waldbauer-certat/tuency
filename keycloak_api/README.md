# KeyCloak API extension

This java module adds an REST endpoint to the KeyCloak which provides a client's base url to a given client id.
Example request:
```http://myKeycloak.org/auth/realms/master/clienturlprovider?clientid=myExampleClient```

The endpoint is needed by the KeyCloak template that are theming the login form, as it does not require authentication.
For this reason the regular KeyCloak admin API can not be used directly.

## Installation

To build and install the module an OpenJDK 11 and a maven installation is needed.

Building the module:

```bash
mvn package
```

Copy the module to the WildFly deploymets:

```bash
cp target/tuency-keycloak-api-extension.jar /path/to/KeyCloak/standalone/deployments
```

The module is then deployed automatically.
