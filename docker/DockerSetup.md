# Docker Setup

## Development Setup

To create a dockerized development environment, the following can be used:

```bash
#Go to project root directory
cd /path/to/project/tuency

#Create a network
docker network create tuency_network

#Build images
docker build -f docker/db/Dockerfile -t tuency/db .
docker build -f docker/backend/Dockerfile -t tuency/backend .
docker build -f docker/client/Dockerfile -t tuency/client .
docker build -f docker/keycloak/Dockerfile -t tuency/keycloak .

#Run containers
docker run --name tuency_db -v $PWD/docker/db:/opt/tuency_db --net=tuency_network -d tuency/db
docker run --name tuency_keycloak --net=tuency_network -p 28080:8080 -d tuency/keycloak
docker run --name tuency_backend -v $PWD/backend:/opt/tuency_backend -v $PWD/docker/backend:/opt/tuency_conf --net=tuency_network -p 20080:80 -d tuency/backend
docker run --name tuency_client -v $PWD/docs/examples/tuency_resources:/opt/tuency_resources -v $PWD/client:/opt/tuency_client -v $PWD/docker/client:/opt/tuency_conf --net=tuency_network -p 7080:80 -d tuency/client
```

For an easier access the container hostnames can be added to the host machine's host file

```bash
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_db)" "tuency_db" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_client)" "tuency_client" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_backend)" "tuency_backend" >> /etc/hosts
echo "$(docker inspect -f "{{ .NetworkSettings.Networks.tuency_network.IPAddress }}" tuency_keycloak)" "tuency_keycloak" >> /etc/hosts
```

For a test using multiple portals, add

```
127.0.0.1 tuencyone tuencytwo three
```

to the hosts file. The applications can the be accessed using http://tuencyone:7080 and http://tuencytwo:7080.

The database can be migrated using the php tool in the backend container:
```
docker exec -ti tuency_backend bash
php artisan migrate:fresh --seed
```

Additionally composer can be used to ensure all dependencies are deployed properly:

```bash
composer install
```

To finish the setup the KeyCloak client needs to be imported via the KeyCloak web interface reachable under `http://tuency_keycloak:8080` using the username `admin` and the password `secret`:

* Navigate to `clients`
* Create a new client
* Import `docker/keycloak/tuency_client.json`
* Repeat using `docker/keycloak/tuencyone.json` and `docker/keycloak/tuencytwo.json`
* The imported clients include all necessary settings except client roles. These have to be added using the _roles_ tab in the respective client's settings.
* Necessary roles:
  * portaladmin
* Assign the portaladmin role the the admin user to access the application:
  * Navigate to `Users`
  * Edit admin user
  * Navigate to `Role Mappings` and assign the portaladmin for all clients using the `Client Roles` combobox
