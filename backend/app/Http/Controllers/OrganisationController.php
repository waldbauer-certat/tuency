<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\Tenant;
use App\Models\OrganisationTag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use App\Auth\KeycloakUser;

class OrganisationController extends Controller
{
    /**
     * Create a pattern for LIKE that matches a given substring.
     *
     * This method replaces '_' with '\_' and '%' with '\%' so that the
     * database does not interpret them as wild-cards and then surrounds
     * the result with '%' wild-cards so that it matches a sub-string.
     */
    private function substringPattern(string $literal)
    {
        $escaped = str_replace('_', '\\_', str_replace('%', '\\%', $literal));
        return "%$escaped%";
    }

    /**
     * List organisations.
     *
     * With the optional query parameter 'name' the result is restricted
     * to those organisations whose name contains the value of that
     * parameter as a sub-string using a case insensitive match.
     *
     * With the optional query parameter 'ancestor', which if given must be
     * the ID of an organisation, the result is restricted to that
     * organisation and its direct and indirect suborganisations.
     *
     * With the optional query parameter 'tenant', which if given must be the
     * ID of an tenant, the result is restricted to organisations that are
     * associated with that tenant, directly or indirectly via their parents.
     *
     * If the optional query parameter 'page' is given the result is
     * paginated using Laravel's standard pagination. The value of the
     * page parameter should be an integer. Page numbering starts from 1.
     *
     * The result is a JSON object with the following attributes:
     *
     *  data: The JSON array with the organisations that were found
     *  meta: A JSON object with pagination meta data. In particular, it
     *        has an attribute 'total' with the total number of organisations.
     */
    public function index(Request $request)
    {
        $validated = $request->validate([
            'name' => 'string|nullable',
            'ancestor' => 'int|nullable',
            'tenant' => 'int|nullable',
            'page' => 'int|nullable',
            'row' => 'int|nullable',
            'sort_direction' => 'string|nullable|in:desc,asc',
            'tag' => [
                'integer',
                function ($attribute, $value, $fail) {
                    if (is_null(OrganisationTag::forUser(Auth::user())->find($value))) {
                        $fail('Unknown contact tag');
                    }
                }
            ],
         ]);
        $this->logRequest($validated);

        $name = $validated['name'] ?? '';
        $page = $validated['page'] ?? -1;
        $tag = $validated['tag'] ?? -1;
        $row = $validated['row'] ?? 10;
        $sortDirection = $validated['sort_direction'] ?? 'asc';

        $query = Organisation::querySubHierarchy(
            Auth::user(),
            $validated['ancestor'] ?? null,
            $validated['tenant'] ?? null,
        );

        if ($tag >= 0) {
            $query = $query->forTag($tag);
        }

        if ($name !== '') {
            $query = $query->where(
                'organisation.name',
                'ilike',
                $this->substringPattern($validated['name'])
            );
        }

        $query->with([
            'tags' => function ($query) {
                $query->forUser(Auth::user());
            },
        ]);

        $query = $query->orderBy('name', $sortDirection);

        $query = $query->select(
            'organisation.organisation_id',
            'organisation.name',
            'organisation.parent_id',
            'organisation.updated_by',
            'organisation.created_at',
            'organisation.updated_at',
        )->distinct();

        // Count the real number of the organization.
        // paginate() unfortunately ignores the distinct() function when
        // counting.
        $count = $query->distinct()->count(
            'organisation.organisation_id',
            'organisation.name',
            'organisation.parent_id'
        );

        if ($page > 0) {
            $orgasPag = $query->paginate($row, ['*'], 'page', $page);
            $orgas = $orgasPag->items();
        } else {
            $orgas = $query->get();
        }

        // Retrieve the the names of the parent-nodes
        foreach ($orgas as $o) {
            $id = $o['organisation_id'];
            $o->parents = Organisation::getParents($id);
            $o->tenants;
        }

        return [
            'data' => $orgas,
            'meta' => [
                'total' => $count,
            ],
        ];
    }

    /**
     * Create a new organisation.
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string',
            'tenant_ids' => 'array|required_without:parent_id',
            'tenant_ids.*' => 'int',
            'parent_id' => 'int|required_without:tenant_ids',
            'tags.*' => ['integer', Rule::exists('organisation_tag', 'organisation_tag_id')],
        ]);

        $validator->after(function ($validator) {
            $validated = $validator->validated();

            if (array_key_exists('tenant_ids', $validated)) {
                Gate::authorize('tenant-as-parent');

                $user = Auth::user();

                // Check if if the current user is allowed to set the tenant_ids.
                if (!Tenant::checkAuthorisation($user, $validated['tenant_ids'])) {
                    $validator->errors()->add(
                        'tenant_ids',
                        'No access permission on one or more tenants or they do not exist.'
                    );
                }
            }

            if (array_key_exists('parent_id', $validated)) {
                Gate::authorize('orga-as-parent');
                // Check if the current user is allowed to set this parent_id
                // value to avoid Mass Assignment
                $user = Auth::user();
                $query = Organisation::forUser($user);
                $allowedIds = $query->pluck('organisation.organisation_id')->toArray();
                if (!in_array($validated['parent_id'], $allowedIds)) {
                    $validator->errors()->add(
                        'parent_id',
                        'No access authorisation to parent.'
                    );
                }
            }
        });

        $validated = $validator->validate();
        $this->logRequest($validated);

        // Get organisation_tags from the parent organisation
        if (array_key_exists('parent_id', $validated)) {
            $parentTags = DB::table('organisation_organisation_tag')
                ->where('organisation_id', $validated['parent_id'])
                ->pluck('organisation_tag_id')->toArray();
        }
        // Add organisation_tags of the parent organisation
        if (!empty($parentTags)) {
            if (array_key_exists('tags', $validated)) {
                // Only portal and tenantadmins are allowed to set new tags.
                Gate::authorize('assign-organisation-tags');
                $validated['tags'] = array_unique(array_merge($validated['tags'], $parentTags));
            } else {
                $validated['tags'] = $parentTags;
            }
        }

        return DB::transaction(function () use ($validated) {

            if (array_key_exists('parent_id', $validated)) {
                $organisation = Organisation::create([
                    'name' => $validated['name'],
                    'parent_id' => $validated['parent_id']
                ]);
            } else {
                $organisation = Organisation::create([
                    'name' => $validated['name']
                ]);
            }

            if (array_key_exists('tenant_ids', $validated)) {
                $tenantIds = $validated['tenant_ids'];
            } else {
                //Get tenant_ids from parent organisation if not given.
                $tenantIds  = DB::table('organisation_tenant')
                    ->where('organisation_id', $validated['parent_id'])
                    ->pluck('tenant_id');
            }

            $organisation->tenants()->sync($tenantIds);

            if (array_key_exists('tags', $validated)) {
                $this->syncTags($organisation, Auth::user(), $validated['tags']);
            }
            return $organisation;
        });
    }

    /**
     * Retrieve a specific organisation
     */
    public function show(Organisation $organisation)
    {
        $this->logRequest();
        return $organisation;
    }

    /**
     * Update an organisation
     */
    public function update(Request $request, Organisation $organisation)
    {
        $validator = Validator::make($request->all(), [
            "name" => "string",
            'tenant_ids' => 'array|filled',
            'tenant_ids.*' => 'int',
            'tags.*' => ['integer', Rule::exists('organisation_tag', 'organisation_tag_id')],
        ]);

        $validator->after(function ($validator) {
            $validated = $validator->validated();

            if (array_key_exists('tenant_ids', $validated)) {
                Gate::authorize('tenant-as-parent');

                $user = Auth::user();

                // Check if if the current user is allowed to set the tenant_ids.
                if (!Tenant::checkAuthorisation($user, $validated['tenant_ids'])) {
                    $validator->errors()->add(
                        'tenant_ids',
                        'No access permission on one or more tenants or they do not exist.'
                    );
                }
            }
        });

        $validated = $validator->validate();
        $this->logRequest($validated);


        return DB::transaction(function () use ($validated, $organisation) {
            $organisation->update($validated);
            if (array_key_exists('tenant_ids', $validated)) {
                $organisation->tenants()->sync($validated['tenant_ids']);
            }
            if (Gate::allows('assign-organisation-tags')) {
                $this->syncTags($organisation, Auth::user(), $validated['tags'] ?? []);
            }
            return $organisation;
        });
    }



    public function syncTags(
        Organisation $organisation,
        KeycloakUser $user,
        $desiredTags
    ) {
        // Make sure we're dealing with ints. The client uses
        // multipart/form-data for some requests and there the tags may be
        // given as strings.
        $desiredTags = array_map('intval', $desiredTags);

        // Assignable are all tags that the user may assign and that belong to
        // one of the tenants the organisation belongs to.
        $assignableTags = OrganisationTag::forUser($user)
            ->whereHas('tenants', function ($query) use ($organisation) {
                $query->whereIn('tenant.tenant_id', $organisation->tenants()->pluck('tenant.tenant_id'));
            })->get();

        // The assigned tags are the tags already assigned to the organisation
        $assignedTags = $organisation->tags()->get();

        // The tags the organisation already has and that the user may not assign
        // must be kept as the user may not modify those.
        $keepTags = $assignedTags->diff($assignableTags);

        // The new set of tags is the union of the kept tags and the
        // intersection of the desired tags and the assignable tags. $keepTags
        // and $assignableTags are disjoint so we can just concat them
        $newTags = $keepTags->concat($assignableTags->only($desiredTags));

        $organisation->tags()->sync($newTags);
    }


    /**
     * Remove an organisation
     */
    public function destroy(Organisation $organisation)
    {
        $this->logRequest();

        // We can only delete the user if we can delete the whole
        // sub-hierarchy, so check whether the usercan see all of it.
        //
        // NOTE: Currently, if a user can see the parent organisation they can
        // always see the sub-organisations, so this check is really
        // necessary. It's still there because the visibility rules may change.
        $fullSubhierarchy = Organisation::queryAncestors()
            ->where('ancestor_id', '=', $organisation->getKey())
            ->distinct('organisation.organisation_id')
            ->get();
        $userSubhierarchy = Organisation::querySubHierarchy(Auth::user(), $organisation->getKey(), null)
            ->distinct('organisation.organisation_id')
            ->get();
        $unremovable = $fullSubhierarchy->diff($userSubhierarchy);

        abort_unless(
            $unremovable->isEmpty(),
            403,
            'The organisation has (indirect) sub-organisations that cannot be removed'
        );

        // Delete the organisation hierarchy, starting with the ones at the
        // bottom.
        $sortedOrgs = $this->sortOrganisationHierarchy($organisation, $fullSubhierarchy);
        DB::transaction(function () use ($sortedOrgs) {
            foreach ($sortedOrgs as $org) {
                $org->delete();
            }
        });
    }

    public function sortOrganisationHierarchy($topOrg, $fullHierarchy)
    {
        $children = collect([]);
        foreach ($fullHierarchy as $org) {
            $children->put($org->getKey(), collect([]));
        }
        foreach ($fullHierarchy as $org) {
            if (!is_null($siblings = $children->get($org->parent_id))) {
                $children->put($org->parent_id, $siblings->push($org));
            }
        }

        $fromTop = collect([]);
        $todo = collect([$topOrg]);
        while (!is_null($cur = $todo->shift())) {
            $fromTop->push($cur);
            $todo = $todo->concat($children->get($cur->getKey()));
        }
        return $fromTop->reverse()->values();
    }
}
