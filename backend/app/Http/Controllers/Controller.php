<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    /**
     * Log information about the current request to the audit log.
     *
     * The log entry will be written to the 'auditlog' channel with log-level
     * 'info' and includes the keycloak user ID of the current user (if
     * authenticated) and the HTTP request method and path. The optional
     * parameter should be an array with additional information about the
     * request, such as parameter taken from e.g. a JSON payload. This array
     * is passed through to the log as contextual data that will be included
     * in the log message.
     */
    public function logRequest($info = null)
    {
        $request = request();
        $method = $request->method();
        $path = $request->path();

        $user = Auth::user();
        $userId = is_null($user) ? 'unauthenticated' : $user->getKeycloakUserId();

        Log::channel('auditlog')->info(
            "TuencyAudit: {$userId} {$method} {$path}",
            $info ?? [],
        );
    }
}
