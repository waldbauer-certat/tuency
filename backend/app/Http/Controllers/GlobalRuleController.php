<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\GlobalRule;
use App\Http\Requests\RuleFormRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Log;

class GlobalRuleController extends Controller
{
    /**
     * Retrieve all rules associated with an ASN.
     */
    public function index()
    {
        $this->logRequest();

        return GlobalRule::with([
            'classificationTaxonomy',
            'classificationType',
            'feedProvider',
            'feedName',
        ])->orderBy('global_rule_id')->get();
    }

    /**
     * Create a new rule associated with an ASN
     */
    public function store(RuleFormRequest $request)
    {
        Gate::authorize('manage-global-rules');

        $validated = $request->validated();
        $this->logRequest($validated);
        return GlobalRule::create($validated);
    }

    /**
     * Retrieve a specific rule
     */
    public function show(GlobalRule $globalRule)
    {
        return $globalRule;
    }

    /**
     * Update a rule.
     */
    public function update(RuleFormRequest $request, GlobalRule $globalRule)
    {
        Gate::authorize('manage-global-rules');

        $validated = $request->validated();
        $this->logRequest($validated);
        $globalRule->update($validated);
        return $globalRule;
    }

    /**
     * Remove a rule.
     */
    public function destroy(GlobalRule $globalRule)
    {
        Gate::authorize('manage-global-rules');

        $this->logRequest();
        $globalRule->delete();
    }
}
