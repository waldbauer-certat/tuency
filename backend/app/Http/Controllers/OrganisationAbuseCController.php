<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Http\Controllers;

use App\Models\Organisation;
use App\Models\RipeHandle;
use App\Models\ContactAutomatic;

class OrganisationAbuseCController extends Controller
{
    /**
     * Return the preferred abuse-c email address.
     *
     * For details on how this address is determined, see the
     * Organisation::preferredAbuseC method.
     *
     * If no abuse-c address exists, e.g. because no RIPE handles have been
     * claimed yet, a 404 response is generated.
     */
    public function index(Organisation $organisation)
    {
        $this->logRequest();

        $abuseC = $organisation->preferredAbuseC();

        abort_if(is_null($abuseC), 404);

        return ['email' => $abuseC];
    }
}
