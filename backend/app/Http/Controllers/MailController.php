<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\ClaimUpdated;
use App\Mail\ConfigReminder;
use App\Models\Contact;
use App\Models\Organisation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Scito\Laravel\Keycloak\Admin\Facades\KeycloakAdmin;
use DateTime;
use DB;
use Illuminate\Support\Facades\Log;

/**
 * Controller handling mails send by the backend
 */
class MailController extends Controller
{
    public static function openKeycloakConnection()
    {
        $clientRealm = config('tuency.keycloak_admin_tuency_realm');
        if ($clientRealm === null) {
            Log::error("tuency.keycloak_admin_tuency_realm not set");
            abort(500);
        }

        return KeycloakAdmin::realm($clientRealm);
    }

    /**
     * Checks if reminder mails have to be sent and sends them.
     */
    public function checkForReminder()
    {
        Log::debug("Checking for expired contacts");
        $interval = config('tuency.mail_contact_expire_interval');
        $date = new DateTime();
        $date->modify("-" . $interval . " day");

        //Query contacts
        $contacts = Contact::whereDate('updated_at', '<=', $date)
                ->get();
        if ($contacts->isEmpty()) {
            Log::debug("No expired contacts found");
            return;
        }
        $realm = self::openKeycloakConnection();
        $users = $realm->users();
        $mails = array();
        Log::debug("Expired contacts found");
        foreach ($contacts as $contact) {
            //Get admin ids
            $admins = $this->getOrgaAdmins($contact->organisation);
            Log::debug("Admins:");
            Log::debug($admins);
            $tenants = $this->getTenants($contact->organisation);
            foreach ($admins as $admin) {
                Log::debug($admin);
                $kcUser = $users->get($admin)->toRepresentation();
                foreach ($tenants as $tenant) {
                    $to_address = $kcUser->getEmail();
                    $to_name = $kcUser->getFirstName() . " " . $kcUser->getLastName();
                    $constituency = str_replace(".", "_", $tenant["name"]);
                    $from_address = $tenant->from_address;
                    $from_name = $tenant->from_name;
                    $reply_to = $tenant->reply_to;
                    $mail = array(
                        "to_address" => $to_address,
                        "to_name" => $to_name,
                        "constituency" => $constituency,
                        "from_address" => $from_address,
                        "from_name" => $from_name,
                        "reply_to" => $reply_to,
                    );
                    array_push($mails, $mail);
                }
            }
        }
        //Send mails
        foreach ($mails as $mail) {
            Log::debug($mail);
            try {
                $this->sendReminder(
                    $mail["to_address"],
                    $mail["to_name"],
                    $mail["constituency"],
                    $mail["from_address"],
                    $mail["from_name"],
                    $mail["reply_to"]
                );
            } catch (\Exception $e) {
                Log::warning("Sending mail failed: " . $e);
            }
        }
    }

    /**
     * Get admin user ids for the given orga.
     * If no admins can be found, parent organisations, if available, will be checked recursively
     */
    private function getOrgaAdmins(Organisation $orga)
    {
        //Get admin ids
        $admins = DB::table('organisation_user')
        -> where('organisation_id', $orga->getKey())
        ->pluck('keycloak_user_id');
        //If there no parents, get parent orga
        if (count($admins) == 0) {
            if (!$orga->parent_id) {
                return array();
            }
            $parent = Organisation::find($orga->parent_id);
            return $this->getOrgaAdmins($parent);
        }
        return $admins;
    }

    /**
     * Get tenants for the given orga.
     * If the orga is a suborga, the parent organisation will be checked recursively
     */
    private function getTenants(Organisation $orga)
    {
        if (!$orga->parent_id) {
            return $orga->tenants;
        } else {
            $parent = Organisation::find($orga->parent_id);
            return $this->getTenants($parent);
        }
    }

    /**
     * Send email notifications that a claim has been updated
     */
    public static function notifyClaimOwners(Organisation $orga)
    {
        $orga_id = $orga->getKey();
        $admins = DB::table('organisation_user')
                    ->where('organisation_id', $orga_id)
                    ->pluck('keycloak_user_id');
        $realm = self::openKeycloakConnection();
        $users = $realm->users();
        $mails = [];
        $tenants = $orga->tenants;
        foreach ($admins as $admin) {
            $kcUser = $users->get($admin)->toRepresentation();
            foreach ($tenants as $tenant) {
                $to_address = $kcUser->getEmail();
                $to_name = $kcUser->getFirstName() . " " . $kcUser->getLastName();
                $constituency = str_replace(".", "_", $tenant["name"]);
                $from_address = $tenant->from_address;
                $from_name = $tenant->from_name;
                $reply_to = $tenant->reply_to;
                $mail = array(
                    "to_address" => $to_address,
                    "to_name" => $to_name,
                    "constituency" => $constituency,
                    "from_address" => $from_address,
                    "from_name" => $from_name,
                    "reply_to" => $reply_to,
                );
                array_push($mails, $mail);
            }
        }
        //Send mails
        foreach ($mails as $mail) {
            Log::debug($mail);
            try {
                self::sendClaimUpdated(
                    $mail["to_address"],
                    $mail["to_name"],
                    $mail["constituency"],
                    $mail["from_address"],
                    $mail["from_name"],
                    $mail["reply_to"]
                );
            } catch (\Exception $e) {
                Log::warning("Sending mail failed: " . $e);
            }
        }
    }

    /**
     * Send a mail that a users claim has been updated
     * Parameters:
     *  - email: User email address to send reminder to
     *  - fullname: Full user name
     *  - constituency: Sending constituency name
     *  - from_address: Sending address
     *  - from_name: Sender name
     *  - reply_to: Reply to address
     */
    private static function sendClaimUpdated(
        string $email,
        string $fullname,
        string $constituency,
        string $from_address = null,
        string $from_name = null,
        string $reply_to = null
    ) {
        Mail::to($email)->send(new ClaimUpdated($fullname, $constituency, $from_address, $from_name, $reply_to));
    }

    /**
     * Send a reminder email to update a user config
     * Parameters:
     *  - email: User email address to send reminder to
     *  - fullname: Full user name
     *  - constituency: Sending constituency name
     *  - from_address: Sending address
     *  - from_name: Sender name
     *  - reply_to: Reply to address
     */
    private function sendReminder(
        string $email,
        string $fullname,
        string $constituency,
        string $from_address = null,
        string $from_name = null,
        string $reply_to = null
    ) {
        Mail::to($email)->send(new ConfigReminder($fullname, $constituency, $from_address, $from_name, $reply_to));
    }
}
