<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Auth\KeycloakGuard;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $auth = $this->app->make("auth");
        $auth->extend("keycloak", function ($app, $name, $config) {
            return new KeycloakGuard($this->app['request']);
        });
    }
}
