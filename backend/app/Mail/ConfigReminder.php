<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class ConfigReminder extends Mailable
{
    use Queueable;
    use SerializesModels;

    /**
     * Recipient fullname
     */
    protected $fullname;

    protected $constituency;

    protected $sender;

    protected $sender_name;

    protected $reply_to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        string $fullname,
        string $constituency,
        string $sender = null,
        string $sender_name = null,
        string $reply_to = null
    ) {
        $this->fullname = $fullname;
        $this->constituency = $constituency;
        $this->sender = $sender;
        $this->sender_name = $sender_name;
        $this->reply_to = $reply_to;
    }

    /**
     * Build the message.
     *
     * The template used will be determined by the given constituency name.
     * E.g., a mail send by the constituency "myconstituency" will be using the template
     * "emails.configreminder_myconstituency"     *
     * The templates can be placed at resources/views/emails.
     *
     * @return $this
     */
    public function build()
    {
        $viewname = 'emails.configreminder_' . $this->constituency;
        //If no from is given, use default
        if (empty($this->sender)) {
            $this->sender = config('mail.from.address');
        }
        if (empty($this->sender_name)) {
            $this->sender_name = $this->constituency;
        }
        if (empty($this->reply_to)) {
            $this->reply_to = $this->sender;
        }
        Log::debug(
            sprintf(
                "Using mail Template %s from %s(%s, reply to: %s) to %s",
                $viewname,
                $this->sender,
                $this->sender_name,
                $this->reply_to,
                $this->fullname
            )
        );
        return $this
            ->from($address = $this->sender, $name = $this->sender_name)
            ->replyTo($address = $this->reply_to, $name = $this->sender_name)
            ->subject('Contact information expired')
            ->with([
                'fullname' => $this->fullname
            ])
            ->view($viewname);
    }
}
