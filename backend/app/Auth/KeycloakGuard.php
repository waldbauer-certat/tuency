<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */


namespace App\Auth;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Auth\GuardHelpers;
use Illuminate\Support\Facades\Log;

/**
 * Guard for KeyCloak based authorisation in tuency.
 */
class KeycloakGuard implements Guard
{
    use GuardHelpers;

    public function __construct(Request $request)
    {
        Log::debug("\$request-headers: $request->headers");
        $keycloakUserId = $request->headers->get("x-sub");
        $username = $request->headers->get("x-user-id-username");
        $groups = $request->headers->get("x-user-id-group");
        if ($keycloakUserId !== null && $username !== null && $groups !== null) {
            $this->user = new KeycloakUser(
                $keycloakUserId,
                $username,
                explode(",", $groups),
                $request->headers->get("x-keycloak-client_id"),
                $request->headers->get("x-user-id-fullname")
            );
        }
    }

    public function user()
    {
        return $this->user;
    }

    // The interface required this method, but we cannot really
    // implement it, so we log when it is called so we can notice if
    // it's actually called in our setup.
    public function validate(array $credentials = [])
    {
        Log::debug("KeycloakGuard::validate", ['credentials' => $credentials]);
    }
}
