<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

trait LogsChanges
{
    protected static function bootLogsChanges()
    {
        static::created(function ($model) {
            $model->logChanges('C');
        });

        static::updated(function ($model) {
            $model->logChanges('U');
        });

        static::deleted(function ($model) {
            $model->logChanges('D');
        });
    }

    public function logChanges($action)
    {
        $whitelist = config('tuency.auditlog')[$this->getTable()] ?? null;

        if (!is_null($whitelist)) {
            if ($action === 'C') {
                // getChanges returns an empty array for newly created models
                // so we use getAttributes instead which gives us all
                // attributes
                $changes = $this->getAttributes();
            } else {
                $changes = $this->getChanges();
            }

            $details = [];
            foreach ($changes as $column => $value) {
                if ($whitelist[$column] ?? false) {
                    $details[$column] = $value;
                }
            }

            $userId = Auth::user()->getKeycloakUserId();
            $table = $this->getTable();
            // pivot tables do not have an ID as the primary key, so use -1 to
            // avoid NULLs
            $changedId = ($this instanceof Pivot) ? -1 : $this->getKey();

            Log::channel('auditlog')->info(
                "TuencyAudit: {$userId} {$action} {$table} {$changedId}",
                $details,
            );

            ChangeLog::create([
                'changed_by' => $userId,
                'action' => $action,
                'changed_table' => $table,
                'changed_id' => $changedId,
                'details' => $details,
            ]);
        }
    }
}
