<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ChangeLog extends Model
{
    protected $table = 'changelog';

    protected $primaryKey = 'changelog_id';

    public $timestamps = false;

    protected $fillable = [
        'changed_by',
        'changed_table',
        'changed_id',
        'action',
        'details',
    ];

    protected $casts = [
        'details' => 'array',
    ];
}
