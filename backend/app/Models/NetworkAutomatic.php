<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NetworkAutomatic extends Model
{
    // We use composite keys for the relation ship with NetworkAutomaticRule
    use \Awobaz\Compoships\Compoships;

    protected $table = 'network_automatic';

    protected $primaryKey = 'network_automatic_id';

    // The tables managed by the RIPE importer have timestamps that are
    // different from the ones Laravel expects.
    public $timestamps = false;

    protected $fillable = [
        'address',
        'import_source',
        'import_time',
    ];

    /**
     * An automatic network could belong to many organisations.
     */
    public function organisations()
    {
        return $this->belongsToMany(
            OrganisationAutomatic::class,
            'organisation_to_network_automatic',
            'network_automatic_id',
            'organisation_automatic_id'
        );
    }

    public function rules()
    {
        return $this->hasMany(
            NetworkAutomaticRule::class,
            ['address', 'import_source'],
            ['address', 'import_source']
        );
    }
}
