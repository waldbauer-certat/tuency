<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

/**
 * Custom pivot model for the Contact <-> NetworkAutomaticRule relationship
 *
 * The purpose of the model is to automatically update the timestamps on the
 * NetworkAutomaticRule model when a contact is attached or detached to/from
 * the network rule.
 */
class ContactNetworkAutomaticRule extends Pivot
{
    use LogsChanges;

    protected $touches = ['networkAutomaticRule'];

    public function networkAutomaticRule()
    {
        return $this->belongsTo(NetworkAutomaticRule::class, 'network_automatic_rule_id');
    }
}
