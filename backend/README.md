# README for the backend of tuency

## Development on a Debian system

### Install Debian dependencies

Tested on Debian 10 (Buster):

    # Direct development dependencies of the backend:
    apt-get install php composer php-cli php-common php-curl php-json php-mbstring php-xml php-pgsql php-bcmath

    # Database
    apt-get install postgresql


### Create Database

As user `postgres`, assuming you use the default cluster:

    createdb tuencydb
    createuser tuency --pwprompt

### Configuration of contact roles and their fields

The contact roles are configured in [config/tuency.php](config/tuency.php). The
instructions for this are also there.

### Mail Configuration

Using the .env file the mail server used for serving notifications or reminder mails can be configured.
The respective entries are:

```
MAIL_MAILER=...
MAIL_HOST=...
MAIL_PORT=...
MAIL_USERNAME=...
MAIL_PASSWORD=...
MAIL_ENCRYPTION=...
MAIL_FROM_ADDRESS=...
MAIL_FROM_NAME=...
```

Additionally the interval in days for contact update reminder can be set:

```
MAIL_CONTACT_EXPIRE_INTERVAL=...
```

To schedule the reminder mails a cronjob is needed, e.g.:

```
* * * * * cd /path/to/tuency/backend && php artisan schedule:run >> /dev/null 2>&1
```

#### Mail Templates

Mails will be sent using blade templates located at `resources/views/emails`.
As the mails are customized per constituency, each constituency needs its own set of mail templates.
E.g. mails for the constituency "example" will be sent using the templates `configreminder_example.blade.php`, `claimupdated_example.blade.php` etc.
If the constituency name contains `.` characters, these need to be replaced by `_`, e.g.:
Mails for the constituency `example.org` will be sent using the template `configreminder_example_org.blade.php`.

### Starting with a clean checkout

    cd backend
    composer install
    cp dot.env.example .env
    php artisan key:generate

    # Edit .env to adapt database and keycloak parameters
    # If you want to access the query API used by IntelMQ,
    # add an INTELMQ_QUERY_TOKEN as described in
    # config/tuency.php as well

    # Initialize the database
    php artisan migrate

    # Start development server (adjust port and host as needed)
    php artisan serve --port=8080 --host=0.0.0.0

    # Small test to see if the server is running.
    # (This API endpoint can be queried unauthorized and returns only static content.)
    curl -H "Accept: application/json" http://localhost:8080/api/uptest

    # For API endpoints requiring authorization, you can set the
    # necessary HTTP request header fields like this:
    curl -H "X-USER-ID-username: admin" \
         -H "X-SUB: curl-test-user" \
         -H "X-USER-ID-GROUP: portaladmin" \
         -H "X-KEYCLOAK-CLIENT_ID: tuencyone" \
         -H "Accept: application/json" \
         http://localhost:8080/api/config

#### Creating testing data

```sh
SEEDER_NUMBER_OF_ORGS=8 \
SEEDER_NUMBER_OF_CONTACTS_PER_ORGA=3 \
SEEDER_NUMBER_OF_NETWORKS_PER_ORGA=3 \
SEEDER_NUMBER_OF_NETWORK_RULES_PER_NETWORK=1 \
php artisan db:seed --help

#creating user accounts (with keycloak and db, thus not default)
#  see routes/console.php for details
php artisan users:seed 7 'example+t5{$i}@tuency-test.ntvtn.de'
```

#### rebuild database with test data

```sh
php artisan migrate:fresh --seed
```

### Debugging

 * The development server logs to `storage/logs/laravel.log`.

 * If `APP_DEBUG` is `true`, when errors occur, the HTTP response
   includes some error information such as PHP tracebacks.

## Deployment / Production

 * Check that `APP_DEBUG` is disabled (`false`) as it would risk
   publishing sensitive information
   [1](https://laravel.com/docs/8.x/errors).

**TODO**


## RIPE Data

Tuency can work with data imported into its database from RIPE with the
ripe importer from
[intelmq-certbund-contact](https://github.com/Intevation/intelmq-certbund-contact).
The RIPE data has to be imported into the same database that tuency uses
for the other netobjects, rules, etc. because the RIPE data can be
associated with the data managed by the users of tuency.


At the time of writing, tuency requires changes to the importer that
have not been included in a release, but which are available in the
`wip-import-routes` branch. In addition to the documentation that comes
with the importer on how to use it, the following tuency specific
information is necessary:

### Route Information

Run the importer with the `--import-route-data` option, because tuency
makes use of the routing information from RIPE and it's not imported by
default.

### Cleanup of RIPE related data

Run the importer with the following option:

    --before-commit-command='SELECT tuency_ripe_cleanup();'

The database function `tuency_ripe_cleanup` performs some cleanup of
data that is managed by tuency and relates to data updated by the
importer.


## LICENSE

The initial version of the backend code was created with `laravel new`.
Laravel's license is identical to the expat license which the FSF
considers GNU GPL compatible, so it should be fine using this as a starting
point for software licensed under GNU AGPL.

The text of Laravel's license can be found in `../LICENSES/MIT.txt`,
substituting "Taylor Otwell" as copyright holder.
