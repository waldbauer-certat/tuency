<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;

use App\Http\Controllers\FqdnController;

/**
 * Test cases for the sub-domain test in the FqdnController
 */
class SubdomainTest extends TestCase
{
    public function testRealSubdomain()
    {
        $this->assertTrue(FqdnController::isSubDomain('example.com', 'ftp.example.com'));
    }

    public function testSubdomainTooShort()
    {
        $this->assertFalse(FqdnController::isSubDomain('www.example.com', 'example.com'));
    }

    public function testSubdomainDifferentDomain()
    {
        $this->assertFalse(FqdnController::isSubDomain('www.intevation.de', 'intevation.org'));
    }

    /**
     * The check for the subdomain must not just check that the parent domain
     * is a suffix of the subdomain.
     */
    public function testSubdomainSameSuffix()
    {
        $this->assertFalse(FqdnController::isSubDomain('www.intevation.de', 'xxxwww.intevation.de'));
    }
}
