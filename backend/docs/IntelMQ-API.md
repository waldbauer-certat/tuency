## API for queries by IntelMQ

### Status of this document

At the time of writing this document is used with issues #119 and #127
to figure out how the API should work. Having it as a separate document
allows us to have one coherent description of the API and its data
structures and algorithms while discussing it in the issues. Once
finished it should serve as the documentation of the API.

### Overview

This document describes the tuency HTTP API which IntelMQ can use to
lookup contact information for events.

### URL

The URL for the API accepts the query parameters described in the table
below. The values of most of the parameters is the value of a field in
the IntelMQ event, and for those the description is simply the name of
the event field.

| Parameter                 | Description/event field                    |
|---------------------------|--------------------------------------------|
| `classification_taxonomy` | `classification.taxonomy`                  |
| `classification_type`     | `classification.type`                      |
| `feed_provider`           | `feed.provider`                            |
| `feed_name`               | `feed.name`                                |
| `feed_status`             | One of the values `"production"`, `"beta"` |
| `ip`                      | The IPv4 or IPv6 address of the event      |
| `domain`                  | The domain the event                       |


Exactly one of the parameters `ip` or `domain` must be given. The other
parameters are all required.

Example URL:

    https://tuency-api-host/intelmq/lookup?classification_taxonomy=availability&classification_type=backdoor&feed_provider=Team+Cymru&feed_name=FTP&feed_status=production&ip=192.168.43.32


### Authentication

Authentication is done with an API key. The client should send that key
in an `Authorization` header field as a `Bearer`-token, e.g.:

    Authorization: Bearer 3JEba0eDP...

The token is configured in the backend's `tuency.php` configuration file.


### Lookup algorithm

1. Lookup IP or domain in manual data

   If found:

      - Iterate through the rules of the organisation associated with
        the ASN in ID order. If a rule matches, continue with "Result"
        below.

   Otherwise:

      - Lookup ASN in automatic data

        This can yield multiple results because the data might have been
        imported from multiple different sources.

2. Global rules

   We should only get here if either there was no manual managed ASN or
   none of the associated rules matched.

   Iterate through the global rules in ID order. If a rule matches, that
   rule is the basis of the result.

3. Result

   At this point we have zero or one rule that matched (either from the
   ASN rules or the global rules) and zero or more ASN entries with
   associated information about organisations.

   The result is a JSON object described below.


### Data structure of the result

In later stages lookups will be done not just for ASNs but also for
other kinds of netobjects, and in order to allow querying for multiple
kinds at once, the result is a JSON object that has one key for each
kind of netobject (currently only `asn` later also e.g. `ip` and
`domain`). The key is only present if there was a matching rule.

The value for each key is a JSON object with the following fields:

| Field          | Description              |
|----------------|--------------------------|
| `suppressed`   | Taken from matched rule  |
| `interval`     | Taken from matched rule. |
| `destinations` | List of destinations     |

Each destination is an object with these fields:

| Field          | Description                                           |
|----------------|-------------------------------------------------------|
| `source`       | String. `"portal"` or the contents of `import_source` |
| `contacts`     | List of Strings. Currently the email addresses        |
| `organisation` | JSON object with the organisation                     |
| `last_updated` | String with ISO formatted time stamp.                 |

The organisation object has the following fields:

| Field            | Description                                                      |
|------------------|------------------------------------------------------------------|
| `id`             | ID of the organisation                                           |
| `constituencies` | Alphabetically sorted list of constituencies of the organisation |
