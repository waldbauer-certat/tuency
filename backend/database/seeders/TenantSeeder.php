<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Tenant;

class TenantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Disable automatic updates of the updated_by column in the seeder.
        // The seeder does not run with a specific Keycloak user, so we cannot
        // meaningfully set that column.
        config(['tuency.set_updated_by' => false]);

        // Disable auditlog. The seeder does not run with a specific Keycloak
        // user, so logging wouldn't work.
        config(['tuency.auditlog' => []]);

        Tenant::insert([
                ['name' => 'cert.mrk'],
                ['name' => 'h20cert.mrk'],
                ['name' => 'windcert.mrk'],
            ]);
    }
}
