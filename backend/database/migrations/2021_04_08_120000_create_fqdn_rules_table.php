<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFqdnRulesTable extends Migration
{
    static $selectors = [
        'classification_taxonomy',
        'classification_type',
        'feed_provider',
        'feed_name',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'fqdn_rule',
            function (Blueprint $table) {
                $table->id("fqdn_rule_id");
                $table->foreignId('fqdn_id')->constrained('fqdn', 'fqdn_id');
                $table->foreignId('classification_taxonomy_id')
                      ->constrained('classification_taxonomy', 'classification_taxonomy_id');
                $table->foreignId('classification_type_id')
                      ->constrained('classification_type', 'classification_type_id');
                $table->foreignId('feed_provider_id')
                      ->constrained('feed_provider', 'feed_provider_id');
                $table->foreignId('feed_name_id')
                      ->constrained('feed_name', 'feed_name_id');
                $table->enum('feed_status', ['production', 'beta', 'any']);
                $table->boolean('suppress');
                $table->integer('interval_length');
                $table->enum('interval_unit', ['immediate', 'hours', 'days', 'weeks', 'month']);
                $table->unique([
                    'fqdn_id',
                    'classification_taxonomy_id',
                    'classification_type_id',
                    'feed_provider_id',
                    'feed_name_id',
                    'feed_status',
                ]);
            }
        );

        Schema::create(
            'contact_fqdn_rule',
            function (Blueprint $table) {
                $table->foreignId('fqdn_rule_id')
                      ->constrained('fqdn_rule', 'fqdn_rule_id');
                $table->foreignId('contact_id')
                      ->constrained('contact', 'contact_id');

                $table->index('fqdn_rule_id');
                $table->unique(['fqdn_rule_id', 'contact_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fqdn_rule');
        Schema::dropIfExists('contact_fqdn_rule');
    }
}
