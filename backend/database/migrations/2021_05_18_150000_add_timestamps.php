<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimeStamps extends Migration
{
    static $tableNames = [
        'asn',
        'asn_rule',
        'asn_rule_contact',
        'classification_taxonomy',
        'classification_type',
        'contact',
        'contact_contact_tag',
        'contact_fqdn_rule',
        'contact_network_rule',
        'contact_organisation_rule',
        'contact_tag',
        'contact_tag_tenant',
        'feed_name',
        'feed_provider',
        'fqdn',
        'fqdn_rule',
        'global_rule',
        'network',
        'network_rule',
        'organisation',
        'organisation_rule',
        'pdf',
        'ripe_org_hdl',
        'tenant',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (static::$tableNames as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (static::$tableNames as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn(['created_at', 'updated_at']);
            });
        }
    }
}
