<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateOrganisationRule extends Migration
{
    static $selectors = [
        'classification_taxonomy',
        'classification_type',
        'feed_provider',
        'feed_name',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (static::$selectors as $selector) {
            $idColumn = "${selector}_id";

            DB::table('organisation_rule')
                ->whereNull($idColumn)
                ->update([$idColumn => -1]);

            Schema::table('organisation_rule', function (Blueprint $table) use ($idColumn) {
                $table->unsignedBigInteger($idColumn)
                    ->nullable(false)
                    ->default(-1)
                    ->change();
            });
        }

        DB::statement("
            ALTER TABLE organisation_rule
            DROP CONSTRAINT  organisation_rule_feed_status_check;
        ");
        DB::statement("
            ALTER TABLE organisation_rule
            ADD CONSTRAINT organisation_rule_feed_status_check
                     CHECK (feed_status = ANY (ARRAY['production', 'beta', 'any']));
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (static::$selectors as $selector) {
            $idColumn = "${selector}_id";

            Schema::table('organisation_rule', function (Blueprint $table) use ($idColumn) {
                $table->unsignedBigInteger($idColumn)
                    ->nullable()
                    ->change();
            });

            DB::table('organisation_rule')
                ->where($idColumn, -1)
                ->update([$idColumn => null]);
        }

        DB::statement("
            ALTER TABLE organisation_rule
            DROP CONSTRAINT  organisation_rule_feed_status_check;
        ");
        DB::statement("
            ALTER TABLE organisation_rule
            ADD CONSTRAINT organisation_rule_feed_status_check
                     CHECK (feed_status = ANY (ARRAY['production', 'beta']));
        ");
    }
}
