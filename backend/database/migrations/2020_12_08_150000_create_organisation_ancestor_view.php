<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganisationAncestorView extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
            CREATE OR REPLACE VIEW organisation_ancestor AS
            WITH RECURSIVE
                org_rec(organisation_id, ancestor_id)
                AS (SELECT organisation_id, organisation_id FROM organisation
                    UNION ALL
                    SELECT r.organisation_id, o.parent_id
                      FROM org_rec r
                      JOIN organisation o ON r.ancestor_id = o.organisation_id
                     WHERE o.parent_id IS NOT NULL)
            SELECT * FROM org_rec;
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW IF EXISTS organisation_ancestor;");
    }
}
