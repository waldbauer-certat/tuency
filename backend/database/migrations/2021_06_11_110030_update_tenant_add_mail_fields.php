<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTenantAddMailFields extends Migration
{
    /**
     * Run the migrations.
     * 
     * Adds the following to the tenant table
     * 
     * column "from_address" [string]
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tenant', function (Blueprint $table) {
            $table->string('from_address', 320)->nullable();
            $table->string('from_name', 320)->nullable();
            $table->string('reply_to', 320)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenant', function (Blueprint $table) {
            $table->dropColumn('from_address');
            $table->dropColumn('from_name');
            $table->dropColumn('replay_to');
        });
    }
}
