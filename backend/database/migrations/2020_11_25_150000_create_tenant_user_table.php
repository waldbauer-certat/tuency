<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTenantUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenant_user', function (Blueprint $table) {
            $table->foreignId('tenant_id')
                ->constrained('tenant', 'tenant_id');
            $table->index('tenant_id');
            $table->uuid('keycloak_user_id');
            $table->index('keycloak_user_id');

            $table->unique(['tenant_id', 'keycloak_user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tenant_user');
    }
}
