<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRuleAbuseC extends Migration
{
    static $ruleTables = [
        'asn_rule',
        'network_automatic_rule',
        'network_rule',
        'organisation_rule',
        'fqdn_rule',

        // global_rule is omitted here because the abuse_c flag doesn't make
        // sense there.
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (self::$ruleTables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->boolean('abuse_c')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (self::$ruleTables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('abuse_c');
            });
        }
    }
}
