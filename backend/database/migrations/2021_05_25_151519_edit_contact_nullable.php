<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class EditContactNullable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->text('email')->nullable()->change();
            $table->text('last_name')->nullable()->change();
            $table->text('phone_1')->nullable()->change();
            $table->text('endpoint')->nullable()->change();
            $table->text('openpgp_pubkey')->nullable()->change();
            $table->text('commentary')->nullable()->change();
            $table->text('first_name')->nullable()->change();
            $table->text('description')->nullable()->change();
            $table->text('phone_2')->nullable()->change();
            $table->text('tel_alarm_mobil')->nullable()->change();
            $table->text('telefon_24_7')->nullable()->change();
            $table->text('street')->nullable()->change();
            $table->text('location')->nullable()->change();
            $table->text('country')->nullable()->change();
            $table->text('s_mime_cert')->nullable()->change();
            $table->text('format')->nullable()->change();
            $table->integer('zip')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->text('email')->nullable(false)->change();
            $table->text('last_name')->nullable(false)->change();
            $table->text('phone_1')->nullable(false)->change();
            $table->text('endpoint')->nullable(false)->change();
            $table->text('openpgp_pubkey')->nullable(false)->change();
            $table->text('commentary')->nullable(false)->change();
            $table->text('first_name')->nullable(false)->change();
            $table->text('description')->nullable(false)->change();
            $table->text('phone_2')->nullable(false)->change();
            $table->text('tel_alarm_mobil')->nullable(false)->change();
            $table->text('telefon_24_7')->nullable(false)->change();
            $table->text('street')->nullable(false)->change();
            $table->text('location')->nullable(false)->change();
            $table->text('country')->nullable(false)->change();
            $table->text('s_mime_cert')->nullable(false)->change();
            $table->text('format')->nullable(false)->change();
            $table->integer('zip')->nullable(false)->change();
        });
    }
}
