<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRipeTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Create the tables used by the RIPE importer. We only create the
         * tables and columns that are actually used by the importer. For each
         * table, the comments show the relevant parts of the SQL schema used
         * by the importer.
         */

        /* network_automatic.
         *
         * Laravel doesn't support the cidr type, so we create this with a raw
         * SQL statement.
         */
        DB::statement("
            CREATE TABLE network_automatic (
                network_automatic_id SERIAL PRIMARY KEY,
                address cidr NOT NULL,
                import_source VARCHAR(500) NOT NULL CHECK (import_source <> ''),
                import_time TIMESTAMP NOT NULL,

                UNIQUE (address, import_source)
            );
        ");

        /* organisation_automatic:
         *
         *     name VARCHAR(500) NOT NULL
         *     ripe_org_hdl VARCHAR(100) NOT NULL DEFAULT ''
         *     import_source VARCHAR(500) NOT NULL
         *     import_time TIMESTAMP NOT NULL
         */
        Schema::create('organisation_automatic', function (Blueprint $table) {
            $table->id('organisation_automatic_id');
            $table->string('name', 500);
            $table->string('ripe_org_hdl', 100);
            $table->string('import_source', 500);
            $table->dateTime('import_time', 100);
        });
        DB::statement("
            ALTER TABLE organisation_automatic
              ADD CONSTRAINT organisation_automatic_import_source_check
                       CHECK (import_source <> '')
        ");

        /* organisation_to_asn_automatic:
         *
         *     organisation_automatic_id INTEGER
         *     asn BIGINT
         *     import_source VARCHAR(500) NOT NULL
         *     import_time TIMESTAMP NOT NULL
         *
         *     PRIMARY KEY (organisation_automatic_id, asn),
         *     FOREIGN KEY (organisation_automatic_id)
         *             REFERENCES organisation_automatic (organisation_automatic_id)
         */
        Schema::create('organisation_to_asn_automatic', function (Blueprint $table) {
            $table->foreignId('organisation_automatic_id')
                ->constrained('organisation_automatic', 'organisation_automatic_id');
            $table->bigInteger('asn');
            $table->string('import_source', 500);
            $table->dateTime('import_time', 100);

            $table->primary(['organisation_automatic_id', 'asn']);
        });
        DB::statement("
            ALTER TABLE organisation_to_asn_automatic
              ADD CONSTRAINT organisation_to_asn_automatic_import_source_check
                       CHECK (import_source <> '')
        ");

        /* organisation_to_network_automatic:
         *
         *     organisation_automatic_id INTEGER
         *     network_automatic_id INTEGER,
         *     import_source VARCHAR(500) NOT NULL
         *     import_time TIMESTAMP NOT NULL
         *
         *     PRIMARY KEY (organisation_automatic_id, network_automatic_id),
         *     FOREIGN KEY (organisation_automatic_id)
         *      REFERENCES organisation_automatic (organisation_automatic_id),
         *     FOREIGN KEY (network_automatic_id)
         *      REFERENCES network_automatic (network_automatic_id)
         */
        Schema::create('organisation_to_network_automatic', function (Blueprint $table) {
            $table->foreignId('organisation_automatic_id')
                ->constrained('organisation_automatic', 'organisation_automatic_id');
            $table->foreignId('network_automatic_id')
                ->constrained('network_automatic', 'network_automatic_id');
            $table->string('import_source', 500);
            $table->dateTime('import_time', 100);

            $table->primary(['organisation_automatic_id', 'network_automatic_id']);
        });
        DB::statement("
            ALTER TABLE organisation_to_network_automatic
              ADD CONSTRAINT organisation_to_network_automatic_import_source_check
                       CHECK (import_source <> '')
        ");

        /* contact_automatic:
         *
         *     email VARCHAR(100) NOT NULL,
         *     organisation_automatic_id INTEGER NOT NULL,
         *     import_source VARCHAR(500) NOT NULL
         *     import_time TIMESTAMP NOT NULL
         *     FOREIGN KEY (organisation_automatic_id)
         *         REFERENCES organisation_automatic (organisation_automatic_id)
         */
        Schema::create('contact_automatic', function (Blueprint $table) {
            $table->id('contact_automatic_id');
            $table->string('email', 100);
            $table->foreignId('organisation_automatic_id')
                ->constrained('organisation_automatic', 'organisation_automatic_id');
            $table->string('import_source', 500);
            $table->dateTime('import_time', 100);
        });
        DB::statement("
            ALTER TABLE contact_automatic
              ADD CONSTRAINT contact_automatic_import_source_check
                       CHECK (import_source <> '')
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_automatic');
        Schema::dropIfExists('organisation_to_network_automatic');
        Schema::dropIfExists('organisation_to_asn_automatic');
        Schema::dropIfExists('organisation_automatic');
        Schema::dropIfExists('network_automatic');
    }
}
