<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* network.
         *
         * Laravel doesn't support the cidr type, so we create this with a raw
         * SQL statement.
         *
         * Where the names of constraints or indexes differ between what
         * postgres would assign and what Laravel would assign, this gives
         * explicit names that follow the conventions of laravel.
         */
        DB::statement("
            CREATE TABLE network (
                network_id SERIAL PRIMARY KEY,
                organisation_id BIGINT NOT NULL
                    CONSTRAINT network_organisation_id_foreign
                    REFERENCES organisation(organisation_id),
                address CIDR NOT NULL,
                approval VARCHAR(255) CHECK (approval = ANY (ARRAY['pending', 'approved', 'denied'])),

                CONSTRAINT network_organisation_id_address_unique UNIQUE (organisation_id, address)
            );
        ");
        DB::statement("
            CREATE INDEX network_address_index ON network USING GIST (address inet_ops);
        ");
        DB::statement("
            CREATE INDEX network_approval_index ON network (approval);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('network');
    }
}
