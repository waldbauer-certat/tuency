<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNetworkAutomaticRuleTable extends Migration
{
    static $selectors = [
        'classification_taxonomy',
        'classification_type',
        'feed_provider',
        'feed_name',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Laravel doesn't support the cidr type, so we create this with a raw
         * SQL statement.
         *
         * Where the names of constraints or indexes differ between what
         * postgres would assign and what Laravel would assign, this gives
         * explicit names that follow the conventions of laravel.
         */
        DB::statement("
            CREATE TABLE network_automatic_rule (
                network_automatic_rule_id SERIAL PRIMARY KEY,
                address CIDR NOT NULL
            );
        ");

        Schema::table('network_automatic_rule', function (Blueprint $table) {
            $table->string('import_source', 500);
            $table->timestamps();
            $table->foreignId('organisation_id')
                ->constrained('organisation', 'organisation_id');
            $table->foreignId('classification_taxonomy_id')
                ->constrained('classification_taxonomy', 'classification_taxonomy_id');
            $table->foreignId('classification_type_id')
                ->constrained('classification_type', 'classification_type_id');
            $table->foreignId('feed_provider_id')
                ->constrained('feed_provider', 'feed_provider_id');
            $table->foreignId('feed_name_id')
                ->constrained('feed_name', 'feed_name_id');
            $table->enum('feed_status', ['production', 'beta', 'any']);
            $table->boolean('suppress');
            $table->integer('interval_length');
            $table->enum('interval_unit', ['immediate', 'hours', 'days', 'weeks', 'month']);
            $table->unique([
                'address',
                'import_source',
                'classification_taxonomy_id',
                'classification_type_id',
                'feed_provider_id',
                'feed_name_id',
                'feed_status',
            ]);
        });

        Schema::create(
            'contact_network_automatic_rule',
            function (Blueprint $table) {
                $table->foreignId('network_automatic_rule_id')
                      ->constrained('network_automatic_rule', 'network_automatic_rule_id');
                $table->foreignId('contact_id')
                      ->constrained('contact', 'contact_id');
                $table->timestamps();

                $table->index('network_automatic_rule_id');
                $table->unique(['network_automatic_rule_id', 'contact_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact_network_automatic_rule');
        Schema::dropIfExists('network_automatic_rule');
    }
}
