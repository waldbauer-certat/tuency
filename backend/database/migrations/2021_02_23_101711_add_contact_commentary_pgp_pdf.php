<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddContactCommentaryPgpPdf extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->text('pgp')
                  ->nullable();
            $table->text('commentary')
                  ->nullable();
            $table->binary('pdf')
                  ->nullable();
            $table->string('email')
                  ->nullable()
                  ->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contact', function (Blueprint $table) {
            $table->dropColumn('pgp');
            $table->dropColumn('commentary');
            $table->dropColumn('pdf');
            $table->string('email')->nullable($value = false)->change();
        });
    }
}
