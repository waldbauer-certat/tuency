<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRuleIntervalSuppress extends Migration
{
    static $ruleTables = [
        'organisation_rule',
        'asn_rule',
    ];
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (static::$ruleTables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->boolean('suppress')->default(false);
                $table->integer('interval_length')->default(0);
                $table->enum(
                    'interval_unit',
                    ['immediate', 'hours', 'days', 'weeks', 'month'],
                )->default('immediate');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (static::$ruleTables as $tableName) {
            Schema::table($tableName, function (Blueprint $table) {
                $table->dropColumn('suppress');
                $table->dropColumn('interval_length');
                $table->dropColumn('interval_unit');
            });
        }
    }
}
