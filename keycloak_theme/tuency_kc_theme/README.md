This keycloak theme as been copied from keycloak-11.0.2
and modified for tuency by Intevation GmbH.

Modified files are marked, all others are still original, and were described as

Copyright 2016 Red Hat, Inc. and/or its affiliates
and other contributors as indicated by the @author tags

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

