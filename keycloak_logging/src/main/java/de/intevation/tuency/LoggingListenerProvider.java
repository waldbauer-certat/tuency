package de.intevation.tuency;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.admin.AdminEvent;
import org.keycloak.events.admin.ResourceType;

/**
 * Listener implementation listening to admin events.
 * Events altering user information will be logged to syslog
 * 
 * @author Alexander Woestmann <awoestmann@intevation.de>
 */
public class LoggingListenerProvider implements EventListenerProvider{

    private static Logger log = LogManager.getLogger(LoggingListenerProvider.class.getName());

    @Override
    public void onEvent(Event e) {
        //Intentionally left blank
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean b) {
        //Only log user events
        if (adminEvent.getResourceType() == ResourceType.USER) {
            String action = adminEvent.getOperationType().toString();
            String realm = adminEvent.getRealmId();
            String user = adminEvent.getAuthDetails().getUserId();
            String representation = adminEvent.getRepresentation();
            log.info(String.format("Realm: %s, User: %s, Action: %s, Representation: %s", realm, user, action, representation));
        }
    }

    @Override
    public void close() {
        //Intentionally left blank
    }
}
