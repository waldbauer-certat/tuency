# KeyCloak API extension

This java module extends KeyCloak by an event listener which logs admin events regarding user account changes to syslog and a log file in the KeyCloak directory `${KEYCLOAK_HOME}/standalone/log/tuency.log`.

## Installation

### Build and deploy the module

To build and install the module an OpenJDK 11 and a maven installation is needed.

Building the module:

```bash
mvn package
```

Copy the module to the WildFly deployments:

```bash
cp target/tuency-keycloak-logging-extension-jar-with-dependencies.jar /path/to/KeyCloak/standalone/deployments
```

The module is then deployed automatically.

### Configure rsyslog

To allow logging to syslog, UDP syslog receptions needs to be enabled.
This can be achieved by adding/editing these lines in `/etc/rsyslog.conf`:

```properties
# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")
```

After editing the config, the rsyslog service needs to be restarted, e.g. by using:

```bash
service rsyslog restart
```
