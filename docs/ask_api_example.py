#!/usr/bin/env python3
"""Example how to access the tuency backend /api in a scripted way.

This file is Free Software under the MIT License
without warranty, see the license for details.

SPDX-License-Identifier: MIT

SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
Software-Engineering: 2021 Intevation GmbH <https://intevation.de>

Author(s):
* Bernhard Reiter <bernhard.reiter@intevation.de>
"""
import html
import logging
import pprint
import re
import requests
import sys


# uncomment only one
#logging.basicConfig(level=logging.DEBUG)
logging.basicConfig(level=logging.INFO)
#logging.basicConfig(level=logging.WARNING)

## uncomment for request debugging as described in
## from https://2.python-requests.org/en/master/api/#api-changes
#from http.client import HTTPConnection
#HTTPConnection.debuglevel = 1

#logging.basicConfig() # you need to initialize logging, otherwise you will not see anything from requests
#logging.getLogger().setLevel(logging.DEBUG)
#requests_log = logging.getLogger("urllib3")
#requests_log.setLevel(logging.DEBUG)
#requests_log.propagate = True
## end of request debugging config


def authenticate(application_url, credentials):
    """Authenticate against the keycloak like a webbrowser would.

    Using explicitly set cookies (and no requests.Session) so it is more
    explicit what is happening regarding the cookies.
    """

    ### Expect to get redirected to keycloak for authentication once
    r1 =  requests.get(application_url)
    # so the first history entry has the request against the application
    logging.info(f"r1.history = {r1.history}")
    logging.info(f"r1.history[0].cookies.keys() = {r1.history[0].cookies.keys()}")
    logging.info(f"r1={r1}")
    logging.info(f"r1.cookies.keys() = {r1.cookies.keys()}")

    keycloak_page = r1.text
    logging.debug(keycloak_page)
    # extract login form action
    form_action = html.unescape(re.search('<form\s[^>]*?\saction="(.*?)"', keycloak_page).group(1))
    logging.info(form_action)

    # login with post, expect to be redirected
    r2 = requests.post(
        url=form_action,
        data=credentials,
        cookies={"AUTH_SESSION_ID": r1.cookies["AUTH_SESSION_ID"]},
        allow_redirects=False
    )

    logging.info(f"r2={r2}")
    if r2.status_code != 302:
        logging.warning("Login failed, did not ge redirected back.")
        raise RuntimeError("Login failed")

    redirect = r2.headers['Location']
    # make sure that we are redirected to the right application
    assert redirect.startswith(application_url)
    logging.info(f"redirect = {redirect}")
    logging.info(f"r2.cookies.keys() = {r2.cookies.keys()}")

    # follow up on the redirect and set the cookies we've got from openresty
    r3 = requests.get(
        url=redirect,
        cookies = {"session": r1.history[0].cookies["session"]},
        allow_redirects=False
        )
    logging.info(f"r3={r3}")
    logging.info(f"r3.cookies.keys() = {r3.cookies.keys()}")

    return r3.cookies


def print_example_data(application_url, credentials):
    try:
        cookie_dict = authenticate(application_url, credentials)
    except (RuntimeError,  requests.exceptions.RequestException) as e:
        logging.warning(e)
        logging.warning("Could not authenticate.")
        sys.exit(1)

    response = requests.get(
        url = application_url + '/api/contacts?page=1&row=3',
        cookies = cookie_dict,
        )

    logging.info(response)
    if response.status_code == 200:
        returned_object = response.json()
        # the paginated return of api/contacts has the real data in `data`
        if "data" in returned_object:
            pprint.pprint(returned_object["data"], compact=True)
    elif response.status_code == 401:
        logging.warning("Not authenticated. Maybe session timed out?")
        # a long running application can try to call authenticate() again
    else:
        logging.error("Something went wrong.")


if __name__ == '__main__':
    application_url = "http://tuencyone"
    credentials = {
        'username' : "orga_42@ntvtn.de",
        'password' : "PASSWORD",
        }

    print_example_data(application_url, credentials)
