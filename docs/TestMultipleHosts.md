Our application shall act as a differently looking portal
when being accessed via a different URL.

## Testing different hostname access behaviour

We want our webbrowser to be able to contact the same server
when being given different hostnames.

 * To let the client yarn based dev-server pass the different hostnames
   the variable `VUE_APP_HOST_CHECK` in the `.env` file
   should be set to true. This may expose your development server a little
   bit more as explained at
   https://webpack.js.org/configuration/dev-server/#devserverdisablehostcheck.


### Use /etc/hosts on the dev-machine

(for Debian GNU/Linux Buster)

If you have root access, you can edit the `/etc/hosts` file
to have several hostnames for the localhost, e.g. that one line reads like
```
127.0.0.1       localhost tuency_backend tuency_keycloak tuencyone tuencytwo tuencythree
```

References:
 * https://man7.org/linux/man-pages/man5/hosts.5.html
 * https://www.gnu.org/software/libc/manual/html_node/NSS-Basics.html

On this machine you will be able to access the localhost host with
these hostnames like `http://tuencyone:8080'.


#### Make openresty use local hostnames
The built-in DNS resolver of openresty will **not** honor `/etc/hosts`
(as of 2020-11-26), so to be able to use the names above from the openidc
internal redirects it is possible
to setup a local dns server with [dnsmasq](https://wiki.debian.org/dnsmasq),
and use `resolver 127.0.0.1` in `nginx.conf`.

```sh
# instructions for Debian GNU/Linux Buster
apt-get install dnsmasq dnsutils

# tighten dnsmasq security to only do dns and listen to localhost
cat <<EOF | patch /etc/dnsmasq.conf
--- /etc/dnsmasq.conf.orig      2020-12-08 09:18:38.612152608 +0100
+++ /etc/dnsmasq.conf   2020-12-08 09:19:02.072174332 +0100
@@ -111 +111 @@
-#listen-address=
+listen-address=127.0.0.1
@@ -115 +115 @@
-#no-dhcp-interface=
+no-dhcp-interface=
EOF

# test after reboot
shutdown -r now
dig @localhost tuency_keycloak
```


An alternative is to use public dns entries that resolve to localhost,
like http://localhost1.ntvtn.de (and similar for 2,3,4) instead of
`tuency_keycloak` and so on.


### Using SOCKS5 proxy to the dev-machine
If you want to do this from another machine, you also need to have access
to `/etc/hosts` on this machine. Alternatively, you can use ssh
to let the webbrowser proxy to your dev machine before resolving the
domain name of the host.

Example (for Debian Buster with Firefox 78.3.0esr):
```bash
# build up a tunnel to the dev-machine on custom ports, start socks5 proxying
ssh -p 4422 dev-machine -D 9991
```

Reference: https://man7.org/linux/man-pages/man1/ssh.1.html

#### Chromium
An example using a temporary profile (so that other chromium windows
aren't affected):

```bash
chromium --temp-profile --proxy-server="socks5://localhost:9991"
```
(Tested with 83.0.4103.116-1~deb10u3 on Debian GNU/Linux Buster.)


#### Firefox

```bash
firefox --ProfileManager
```
 * create new profile
 * Hamburger Symbol -> Preferences -> (bottom) Network Settings -> Settings:
   * (x) Manual Proxy
     * Socks Host: localhost Port: 9991
     * (x) SOCKS5
   * [x] Proxy DNS when using SOCKS v5

(Tested with 78.3.0esr on Debian GNU/Linux Buster.)

#### curl
```bash
curl --preproxy socks5h://localhost:9991 http://tuencythree
```
(Tested with 7.64.0-4+deb10u1 on  Debian GNU/Linux Buster.)
